package com.nishtahir.linkbait.extensions

/**
 * For temporary file storage
 */
val DEFAULT_TEMP_FILE_DIRECTORY = "data/tmp"

/**
 * Plugins downloaded as artifacts as well as their
 * dependencies will be stored here
 */
val DEFAULT_REPOSITORY = "data/repo"

/**
 * Static files to be served will be stored here
 */
val DEFAULT_STATIC_FILE_DIRECTORY = "data/static"

/**
 * External plugins should be kept here
 */
val DEFAULT_PLUGIN_DIRECTORY = "data/plugins"

/**
 * Data stuff
 */
val DEFAULT_DATA_DIRECTORY = "data"
/**
 * Port number to serve on
 */
val DEFAULT_PORT = 4567